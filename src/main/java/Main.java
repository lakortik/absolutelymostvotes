import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * система голосования по типу относительного большинства
 */
public class Main {

    private static List<Candidat> candidats = new LinkedList<Candidat>(){{
       add(new Candidat("Никита", 0));
       add(new Candidat("Ирина", 0));
       add(new Candidat("Саша", 0));
       add(new Candidat("Вася", 0));
    }};

    public static void main(String[] args) throws Exception {
        System.out.println("Выбор старосты группы. метод относительного большинства");
        System.out.println("Введите количество голосующих людей");


        Scanner scanner = new Scanner(System.in);
        Integer electorCount = scanner.nextInt();

        System.out.println("Голосуйте " + electorCount + " раз(а)");

        //вывод на просмотр
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(i + " - " + candidats.get(i).getName());
        }

        //голосование n раз
        for (int i = 0; i < electorCount; i++){
            Integer vote = scanner.nextInt();

            Candidat voteCandidate = candidats.get(vote);
            voteCandidate.setVotes(voteCandidate.getVotes()+1);
        }

        //вывод на просмотр с количетвом голосов
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(i + " - " + candidats.get(i).getName() + ", количество голосов: " + candidats.get(i).getVotes());
        }

        //поиск максимального количества и вывод этого кандидата
        Candidat maxVoteCandidate = candidats.stream().reduce((a, b) -> a.getVotes() > b.getVotes() ? a : b).get();
        System.out.println("Победил кандидат: " + maxVoteCandidate.getName() + ", так как набрал максимальное количество голосов - " + maxVoteCandidate.getVotes());


    }
}
